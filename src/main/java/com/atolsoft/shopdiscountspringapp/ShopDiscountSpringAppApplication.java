package com.atolsoft.shopdiscountspringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class ShopDiscountSpringAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopDiscountSpringAppApplication.class, args);
	}
}
