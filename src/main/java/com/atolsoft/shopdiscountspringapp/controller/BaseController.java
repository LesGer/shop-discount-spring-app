package com.atolsoft.shopdiscountspringapp.controller;

import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.CartFull;
import com.atolsoft.shopdiscountspringapp.exeptions.InvalidDiscount;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BaseController {

    @ResponseStatus(value = HttpStatus.BAD_REQUEST,
            reason = "Cart is Empty")
    @ExceptionHandler(CartEmpty.class)
    public void emptyCart() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST,
            reason = "Cart is Full")
    @ExceptionHandler(CartFull.class)
    public void fullCart() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST,
            reason = "Invalid object")
    @ExceptionHandler(CartEmpty.class)
    public void badObject() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST,
            reason = "Invalid discount")
    @ExceptionHandler(InvalidDiscount.class)
    public void invalidDiscount() {
    }



}
