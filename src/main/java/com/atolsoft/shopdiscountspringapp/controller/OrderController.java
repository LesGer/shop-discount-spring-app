package com.atolsoft.shopdiscountspringapp.controller;


import com.atolsoft.shopdiscountspringapp.controller.dto.OrderDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.CartFull;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.exeptions.InvalidDiscount;
import com.atolsoft.shopdiscountspringapp.service.OrderService;
import jdk.internal.dynalink.linker.LinkerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/order")
@Validated
public class OrderController extends BaseController {

	@Autowired
    OrderService orderService;

	@GetMapping(value ="/")
	public List<OrderDTO> getAllOrders(){
		return orderService.getAllOrders();
	}
	
	@GetMapping(value="/add/{order}/{product}")
	public OrderDTO addToCart(@Valid @RequestBody OrderDTO order,@Valid @RequestBody ProductDTO product)
			throws CartFull, EmptyObject{
		
		return orderService.addToCart(order, product);
		
	}

	@GetMapping(value="/add/{order}/{id}")
	public OrderDTO addToCart(@Valid @RequestBody OrderDTO order,@Valid @RequestParam int product)
			throws CartFull, EmptyObject{

		return orderService.addToCart(order, product);

	}
	
	@GetMapping(value="/remove/{order}/{product}")
	public OrderDTO removeFromCart(@Valid @RequestBody OrderDTO order,@Valid @RequestBody ProductDTO product)
			throws CartEmpty, EmptyObject{

		return orderService.removeFromCart(order, product);
		
	}
	
	@GetMapping(value="/remove/{order}/{index}")
	public OrderDTO removeFromCart(@Valid @RequestBody OrderDTO order, @Valid @RequestParam int index)
			throws CartEmpty, EmptyObject{

		return orderService.removeFromCart(order, index);
		
	}
	
	@GetMapping(value="/clear/{order}")
	public OrderDTO removeFromCart(@Valid @RequestBody OrderDTO order) throws EmptyObject, CartEmpty{

		return orderService.removeAllFromCart(order);
		
	}
	
	@GetMapping(value="/discount/{order}/{value}")
	public OrderDTO addDiscount(@Valid @RequestBody OrderDTO order, @RequestParam double value)
			throws EmptyObject, InvalidDiscount {

		return orderService.addDiscount(order, value);
		
	}
	
	@PostMapping(value="/save/{order}")
	public void placeOrder(@Valid @RequestBody OrderDTO order) throws CartEmpty, EmptyObject{
		orderService.placeOrder(order);
	}
	
}
