package com.atolsoft.shopdiscountspringapp.controller;



import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product")
public class ProductController extends BaseController{
	
	@Autowired
	ProductService productService;
	
	@GetMapping(value="/all")
	public List<ProductDTO> getAllProducts(){
		
		return productService.getAllProducts();
		
	}
	
	@GetMapping(value="/all/{type}")
	public List<ProductDTO> getAllProductsByType(@RequestParam String type){
		
		return productService.getAllProductsByType(type);
		
	}

	@GetMapping(value="/{id}")
	public ProductDTO getProductById(@RequestParam int index) throws EmptyObject {
		return productService.findByProductId(index);
	}

	@PostMapping(value = "/add")
	public void addProduct(@RequestBody ProductDTO productDTO) throws EmptyObject {
		productService.saveProduct(productDTO);
	}

	@PutMapping(value = "/update/{id}")
	public void updateProduct(@RequestBody ProductDTO productDTO, @RequestParam  int id) throws EmptyObject {
		productService.updateProduct(productDTO,id);
	}

	@DeleteMapping(value = "/delete/")
	public void deleteProuct(@RequestBody ProductDTO productDTO) throws EmptyObject {
		productService.deleteProduct(productDTO);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteProuct(@RequestParam int id) throws EmptyObject {
		productService.deleteProduct(id);
	}

}
