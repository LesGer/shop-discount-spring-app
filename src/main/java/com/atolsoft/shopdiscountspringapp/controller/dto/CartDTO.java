package com.atolsoft.shopdiscountspringapp.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class CartDTO {

	@NotNull
	private List<ProductDTO> productList;
	@Min(0)
	private double cartValue;


	public CartDTO(){
		this.productList = new ArrayList<>();
	}
}
