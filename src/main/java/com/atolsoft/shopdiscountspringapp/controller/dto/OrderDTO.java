package com.atolsoft.shopdiscountspringapp.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@Builder
public class OrderDTO {

	@NotNull
	private CartDTO cart;
	@Min(0)
	private double orderValue;
	@Min(0)
	private double discount;
	@NotBlank
	private String discountType;

	public OrderDTO(){
		this.cart = new CartDTO();
	}

}
