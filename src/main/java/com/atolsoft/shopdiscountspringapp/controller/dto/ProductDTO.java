package com.atolsoft.shopdiscountspringapp.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDTO {


	private int productId;
	@NotBlank
	@Min(3)
	private String name;
	@NotBlank
	@Min(3)
	private String description;
	@NotBlank
	private String type;
	@Min(0)
	private double price;
	@Min(0)
	private double discountValue;

}
