package com.atolsoft.shopdiscountspringapp.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShopDTO {

    private OrderDTO orderDTO;
    private double var;

    public ShopDTO(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }
}
