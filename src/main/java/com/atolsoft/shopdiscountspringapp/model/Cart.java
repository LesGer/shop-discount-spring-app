package com.atolsoft.shopdiscountspringapp.model;


import com.atolsoft.shopdiscountspringapp.model.products.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="cart")
public class Cart {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cart_id")
	private int cartId;
	@ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
        name = "cart_product", 
        joinColumns = { @JoinColumn(name = "cart_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "product_id") }
    )
	private List<Product> productList;
	@Min(0)
	private double cartValue;
	
	
}
