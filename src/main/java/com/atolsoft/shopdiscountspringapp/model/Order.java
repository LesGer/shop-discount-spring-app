package com.atolsoft.shopdiscountspringapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table (name="orders")
public class Order {

	public static final int CART_SIZE = 5;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="order_id")
	@Min(0)
	private int orderId;
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id")
	private Cart cart;
	@Column(name="value")
	private double orderValue;
	@Transient
	private double discount;
	@Transient
	private String discountType;
	
	
}
