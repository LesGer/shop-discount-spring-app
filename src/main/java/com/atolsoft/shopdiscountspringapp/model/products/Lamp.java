package com.atolsoft.shopdiscountspringapp.model.products;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("Lamp")
public class Lamp extends Product {

	public Lamp(){
		setType(ProductType.LAMP.getType());
	}

	public Lamp(int id, String name, String description, double price){
		super(id,name,description,price);
		setType(ProductType.LAMP.getType());
	}


}
