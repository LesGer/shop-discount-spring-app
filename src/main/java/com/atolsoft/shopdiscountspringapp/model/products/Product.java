package com.atolsoft.shopdiscountspringapp.model.products;

import com.atolsoft.shopdiscountspringapp.model.Cart;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table (name="product")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type", 
    discriminatorType = DiscriminatorType.STRING)
public abstract class Product {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="product_id")
	private int productId;
	@NotBlank
	@Size(min=3)
	private String name;
	@NotBlank
	@Size(min=3)
	private String description;
	@Column(insertable = false, updatable = false)
	@NotBlank
	private String type;
	@Min(0)
	private double price;
	@ManyToMany(mappedBy = "productList", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Cart> cart;
	@Transient
	private double discountValue;
	
	public Product(int id, String name, String description, double price){
		this.productId = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.type = this.getClass().getSimpleName();
	}	
}
