package com.atolsoft.shopdiscountspringapp.model.products;

public enum ProductType {

    VEGETABLE ("Vegetable"),
    LAMP ("Lamp");

    private String type;


    ProductType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
