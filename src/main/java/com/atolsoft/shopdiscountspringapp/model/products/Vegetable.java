package com.atolsoft.shopdiscountspringapp.model.products;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("Vegetable")
public class Vegetable extends Product {

	public Vegetable(){
		setType(ProductType.VEGETABLE.getType());
	}

	public Vegetable(int id, String name, String description, double price){
		super(id,name,description,price);
		setType(ProductType.VEGETABLE.getType());
	}

}
