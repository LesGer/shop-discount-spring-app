package com.atolsoft.shopdiscountspringapp.repository;

import com.atolsoft.shopdiscountspringapp.model.Cart;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Integer> {

}
