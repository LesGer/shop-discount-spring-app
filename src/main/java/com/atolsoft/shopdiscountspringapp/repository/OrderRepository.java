package com.atolsoft.shopdiscountspringapp.repository;


import com.atolsoft.shopdiscountspringapp.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {

}
