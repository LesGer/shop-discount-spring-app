package com.atolsoft.shopdiscountspringapp.repository;


import com.atolsoft.shopdiscountspringapp.model.products.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

	List<Product> findAllByType(String type);
	Product findFirstByName(String name);
	
}
