package com.atolsoft.shopdiscountspringapp.service;


import com.atolsoft.shopdiscountspringapp.controller.dto.OrderDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.CartFull;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.exeptions.InvalidDiscount;
import com.atolsoft.shopdiscountspringapp.model.Order;
import com.atolsoft.shopdiscountspringapp.repository.OrderRepository;
import com.atolsoft.shopdiscountspringapp.repository.ProductRepository;
import com.atolsoft.shopdiscountspringapp.service.factory.DiscountFactory;
import com.atolsoft.shopdiscountspringapp.service.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    DiscountFactory discount;
    @Autowired
    ProductService productService;


    public List<OrderDTO> getAllOrders(){
        return StreamSupport.stream(orderRepository.findAll().spliterator(),false)
                .map(o -> orderMapper.mapToDTO(o))
                .collect(Collectors.toList());
    }

    public OrderDTO addToCart(OrderDTO orderDTO, ProductDTO productDTO) throws CartFull, EmptyObject{

        checkNull(orderDTO);
        checkNull(productDTO);

        if (orderDTO.getCart().getProductList().size() < Order.CART_SIZE) {
            orderDTO.getCart().getProductList().add(productDTO);
            updateCartValue(orderDTO);
        } else {
            throw new CartFull();
        }

        return orderDTO;
    }

    public OrderDTO addToCart(OrderDTO orderDTO, int productId) throws CartFull, EmptyObject{

        checkNull(orderDTO);

        if(productId == 0){
            throw new EmptyObject();
        }

        if (orderDTO.getCart().getProductList().size() < Order.CART_SIZE) {

            productService.findByProductId(productId);

            orderDTO.getCart().getProductList().add(productService.findByProductId(productId));
            updateCartValue(orderDTO);

        } else {
            throw new CartFull();
        }
        return orderDTO;
    }

    public OrderDTO removeFromCart(OrderDTO orderDTO, ProductDTO productDTO) throws CartEmpty, EmptyObject {

        checkNull(orderDTO);
        checkNull(productDTO);
        checkIfCartIsEmpty(orderDTO);

        orderDTO.getCart().getProductList().remove(productDTO);
        updateCartValue(orderDTO);
        return orderDTO;
    }

    public OrderDTO removeFromCart(OrderDTO orderDTO, int position) throws CartEmpty, EmptyObject {

        checkNull(orderDTO);
        checkIfCartIsEmpty(orderDTO);

        if ((orderDTO.getCart().getProductList().size()-1) < position) {
            throw new EmptyObject();
        }

        orderDTO.getCart().getProductList().remove(position);
        updateCartValue(orderDTO);
        return orderDTO;

    }

    public OrderDTO removeAllFromCart(OrderDTO orderDTO) throws EmptyObject, CartEmpty {

        checkNull(orderDTO);
        checkIfCartIsEmpty(orderDTO);

        orderDTO.getCart().getProductList().clear();
        orderDTO.setOrderValue(0);
        return orderDTO;

    }

    public OrderDTO addDiscount(OrderDTO orderDTO, double discount) throws EmptyObject, InvalidDiscount {

        checkNull(orderDTO);

        if (discount <= 0) {
            throw new InvalidDiscount();
        }

        orderDTO.setDiscount(orderDTO.getDiscount() + discount);

        return orderDTO;
    }

    public OrderDTO placeOrder(OrderDTO orderDTO) throws CartEmpty, EmptyObject {

        checkNull(orderDTO);
        checkIfCartIsEmpty(orderDTO);

        orderDTO = checkDiscount(orderDTO);

        Order orderToAdd = orderMapper.mapFromDTO(orderDTO);

        orderRepository.save(orderToAdd);

        return orderDTO;
    }

    private OrderDTO checkDiscount(OrderDTO orderDTO) throws EmptyObject, CartEmpty {

        checkNull(orderDTO);
        checkIfCartIsEmpty(orderDTO);

        if (orderDTO.getDiscount() != 0) {

            orderDTO = orderMapper.mapToDTO(discount.setDiscountFromType(
                    orderDTO.getDiscountType()
            ).countDiscount(orderMapper.mapFromDTO(orderDTO)));
        } else {
            orderDTO.setOrderValue(orderDTO.getCart().getCartValue());
        }

        return orderDTO;

    }

    private <T> void checkNull(T t) throws EmptyObject {

        if (t == null) {
            throw new EmptyObject();
        }

    }

    private void checkIfCartIsEmpty(OrderDTO orderDTO) throws CartEmpty {
        if (orderDTO.getCart().getProductList().isEmpty()) {
            throw new CartEmpty();
        }
    }

    private void updateCartValue(OrderDTO orderDTO) {
        BigDecimal cartValue = new BigDecimal(0);

        List<ProductDTO> products = orderDTO.getCart().getProductList();

        for (ProductDTO p : products) {
            cartValue = cartValue.add(BigDecimal.valueOf(p.getPrice()));
        }
        orderDTO.getCart().setCartValue(cartValue.doubleValue());
    }


}
