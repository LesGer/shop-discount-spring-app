package com.atolsoft.shopdiscountspringapp.service;

import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.repository.ProductRepository;
import com.atolsoft.shopdiscountspringapp.service.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	@Autowired
	ProductMapper productMapper;
	
	public List<ProductDTO> getAllProducts(){
		
		return streamToList(productRepository.findAll());
	}
	
	public List<ProductDTO> getAllProductsByType(String type){
		
		return streamToList(productRepository.findAllByType(type));
		
	}

	public ProductDTO findByProductId(int id) throws EmptyObject {

		if(id <= 0){
			throw new EmptyObject();
		}

		Product foundProduct = productRepository.findOne(id);

		checkNull(foundProduct);

		return productMapper.mapToDTO(foundProduct);
	}

	public void saveProduct(ProductDTO dto) throws EmptyObject {

		checkNull(dto);

		Product productToAdd = productMapper.mapToAdd(dto);

		productRepository.save(productToAdd);

	}

	public void updateProduct(ProductDTO dto, int productId) throws EmptyObject {

		checkNull(dto);

		Product productToUpdate = productRepository.findOne(productId);

		checkNull(productToUpdate);

		productRepository.save(productToUpdate);

	}

	public void deleteProduct(ProductDTO dto) throws EmptyObject {

		checkNull(dto);
		ProductDTO productToDelete = findByProductId(dto.getProductId());

		productRepository.delete(productMapper.mapFromDTO(productToDelete));
	}

	public void deleteProduct(int index) throws EmptyObject {

		if(index <= 0) {
			throw new EmptyObject();
		}
		Product product = productRepository.findOne(index);

		checkNull(product);

		productRepository.delete(product);
	}
	
	
	private List<ProductDTO> streamToList(Iterable<Product> iterable){
		return StreamSupport.stream(iterable.spliterator(), false)
				.map(p -> productMapper.mapToDTO(p))
				.collect(Collectors.toList());
	}

	private <T> void checkNull(T t) throws EmptyObject {

		if (t == null) {
			throw new EmptyObject();
		}

	}
}
