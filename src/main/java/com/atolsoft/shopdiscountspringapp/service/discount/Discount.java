package com.atolsoft.shopdiscountspringapp.service.discount;

import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.model.Order;
import org.springframework.stereotype.Component;

@Component
public interface Discount {

	public Order countDiscount(Order order) throws EmptyObject, CartEmpty;
	
}
