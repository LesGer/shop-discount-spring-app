package com.atolsoft.shopdiscountspringapp.service.discount;

public enum DiscountType {

    PROPORTIONAL("Proportional");

    String name;

    DiscountType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
