package com.atolsoft.shopdiscountspringapp.service.discount;

import com.atolsoft.shopdiscountspringapp.model.Order;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class Proportional implements Discount {

    @Override
    public Order countDiscount(Order order) {

        count(order);

        return setOrderPriceAfterDiscount(order);
    }


    private void count(Order order) {

        List<Product> products = order.getCart().getProductList();

        BigDecimal cartValue = new BigDecimal(order.getCart().getCartValue());
        BigDecimal discountValue = new BigDecimal(order.getDiscount());

        BigDecimal productValue;
        BigDecimal discountPercentage;
        BigDecimal remainingDiscount = new BigDecimal(order.getDiscount());

        for (Product p : products) {
            productValue = BigDecimal.valueOf(p.getPrice());

            discountPercentage = productValue.divide(cartValue, 4, BigDecimal.ROUND_HALF_UP);
            p.setDiscountValue(discountPercentage.multiply(discountValue).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

            remainingDiscount = remainingDiscount.subtract(new BigDecimal(p.getDiscountValue()));
        }

        checkRemainingDiscount(remainingDiscount, order);
    }

    private void checkRemainingDiscount(BigDecimal remainingDiscount, Order order) {

        int orderSize = order.getCart().getProductList().size() - 1;
        Product lastProduct = order.getCart().getProductList().get(orderSize);

        if (!remainingDiscount.equals(BigDecimal.ZERO)) {
            lastProduct.setDiscountValue(
                    BigDecimal.valueOf(lastProduct.getDiscountValue()).add(remainingDiscount)
                            .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
        }

    }

    private Order setOrderPriceAfterDiscount(Order order) {

        if (order.getDiscount() > order.getCart().getCartValue()) {
            setFullDiscountToAllProducts(order);

        } else {
            order.setOrderValue(BigDecimal.valueOf(order.getCart().getCartValue())
                    .subtract(BigDecimal.valueOf(order.getDiscount()))
                    .doubleValue());

        }
        return order;
    }

    private void setFullDiscountToAllProducts(Order order) {

        List<Product> products = order.getCart().getProductList();

        for (Product p : products) {
            p.setDiscountValue(p.getPrice());
        }

    }

}

