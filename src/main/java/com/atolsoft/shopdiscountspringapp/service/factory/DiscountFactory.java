package com.atolsoft.shopdiscountspringapp.service.factory;

import com.atolsoft.shopdiscountspringapp.service.discount.Discount;
import com.atolsoft.shopdiscountspringapp.service.discount.DiscountType;
import com.atolsoft.shopdiscountspringapp.service.discount.Proportional;
import org.springframework.stereotype.Component;

@Component
public class DiscountFactory {

    public Discount setDiscountFromType(String discountType){

        if(DiscountType.PROPORTIONAL.getName().equals(discountType)){
            return new Proportional();
        }

        return null;
    }

}
