package com.atolsoft.shopdiscountspringapp.service.factory;

import com.atolsoft.shopdiscountspringapp.model.products.Lamp;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.model.products.ProductType;
import com.atolsoft.shopdiscountspringapp.model.products.Vegetable;
import com.atolsoft.shopdiscountspringapp.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductFactory {

	@Autowired
	ProductRepository productRepository;

	public Product createProductFromType(String type){
		
		if(ProductType.LAMP.getType().equals(type)){
			return new Lamp();
		} else if(ProductType.VEGETABLE.getType().equals(type)){
			return new Vegetable();
		}
		
		return null;
	}

}
