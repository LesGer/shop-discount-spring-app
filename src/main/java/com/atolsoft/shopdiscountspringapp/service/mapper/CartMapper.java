package com.atolsoft.shopdiscountspringapp.service.mapper;


import com.atolsoft.shopdiscountspringapp.controller.dto.CartDTO;
import com.atolsoft.shopdiscountspringapp.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class CartMapper {
	
	@Autowired
	ProductMapper productMapper;
	
	public CartDTO mapToDTO(Cart cart){
		return CartDTO.builder().cartValue(cart.getCartValue())
				.productList(cart.getProductList()
						.stream().map(p -> productMapper.mapToDTO(p)).collect(Collectors.toList()))
				.build();
	}

	public Cart mapFromDTO(CartDTO dto) {
		
		return Cart.builder()
				.cartValue(dto.getCartValue())
				.productList(dto.getProductList()
						.stream().map(p -> productMapper.mapFromDTO(p)).collect(Collectors.toList()))
				.build();
		
	}
	
}
