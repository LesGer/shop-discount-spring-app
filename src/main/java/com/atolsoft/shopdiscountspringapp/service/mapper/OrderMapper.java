package com.atolsoft.shopdiscountspringapp.service.mapper;

import com.atolsoft.shopdiscountspringapp.controller.dto.CartDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.OrderDTO;
import com.atolsoft.shopdiscountspringapp.model.Order;
import com.atolsoft.shopdiscountspringapp.service.discount.DiscountType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class OrderMapper {

	@Autowired
	CartMapper cartMapper;
	@Autowired
	ProductMapper productMapper;
	
	public Order mapFromDTO(OrderDTO dto){
		
		return Order.builder()
				.orderValue(dto.getOrderValue())
				.discount(dto.getDiscount())
				.cart(cartMapper.mapFromDTO(dto.getCart()))
				.discountType(dto.getDiscountType())
				.build();
				
	}
	
	public OrderDTO mapToDTO(Order order){
		
		return OrderDTO.builder().discount(order.getDiscount())
				.cart(cartMapper.mapToDTO(order.getCart()))
				.orderValue(order.getOrderValue())
				.discountType(order.getDiscountType())
				.build();
		
	}

	
	
}
