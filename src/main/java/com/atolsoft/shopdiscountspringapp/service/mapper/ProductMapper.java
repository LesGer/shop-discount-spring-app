package com.atolsoft.shopdiscountspringapp.service.mapper;

import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.service.factory.ProductFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductMapper {
	
	@Autowired
    ProductFactory productFactory;

	public ProductDTO mapToDTO(Product product){
		return  ProductDTO.builder()
				.productId(product.getProductId())
				.name(product.getName())
				.description(product.getDescription())
				.price(product.getPrice())
				.discountValue(product.getDiscountValue())
				.type(product.getType())
				.build();
	}
	
	public Product mapFromDTO(ProductDTO dto){
		Product product = productFactory.createProductFromType(dto.getType());
		product.setProductId(dto.getProductId());
		product.setName(dto.getName());
		product.setDescription(dto.getDescription());
		product.setPrice(dto.getPrice());
		product.setDiscountValue(dto.getDiscountValue());
		
		return product;
	}

	public Product mapToAdd(ProductDTO dto){
		Product product = productFactory.createProductFromType(dto.getType());
		product.setName(dto.getName());
		product.setDescription(dto.getDescription());
		product.setPrice(dto.getPrice());

		return product;
	}

}
