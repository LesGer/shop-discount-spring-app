package com.atolsoft.shopdiscountspringapp.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeViewController {

    @GetMapping({"","/","/home"})
    public String home(){
        return "home";
    }

}
