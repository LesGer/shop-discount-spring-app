package com.atolsoft.shopdiscountspringapp.view;

import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.model.products.ProductType;
import com.atolsoft.shopdiscountspringapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductViewController {

    @Autowired
    ProductService productService;

    @GetMapping("/addProduct")
    public ModelAndView addProduct(){

        ModelAndView modelAndView = new ModelAndView("addProduct");
        modelAndView.addObject("type", ProductType.values());
        modelAndView.addObject("product", new ProductDTO());

        return modelAndView;
    }


    @PostMapping("/saveProduct")
    public String saveProduct(@ModelAttribute ProductDTO productDTO) throws EmptyObject {

        productService.saveProduct(productDTO);

        return "home";
    }

    @GetMapping
    public ModelAndView productList(){

        ModelAndView modelAndView = new ModelAndView("products");
        modelAndView.addObject("products", productService.getAllProducts());

        return modelAndView;

    }

}
