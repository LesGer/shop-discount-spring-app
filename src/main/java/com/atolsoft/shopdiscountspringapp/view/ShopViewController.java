package com.atolsoft.shopdiscountspringapp.view;

import com.atolsoft.shopdiscountspringapp.controller.dto.OrderDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.ShopDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.CartFull;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.exeptions.InvalidDiscount;
import com.atolsoft.shopdiscountspringapp.service.OrderService;
import com.atolsoft.shopdiscountspringapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShopViewController {

    @Autowired
    ProductService productService;
    @Autowired
    OrderService orderService;
    @Autowired
    ShopViewService shopViewService;


    @GetMapping("/shop")
    public ModelAndView shop(){


        ModelAndView modelAndView = new ModelAndView("shop");
        modelAndView.addObject("shopOrder", new ShopDTO());
        modelAndView.addObject("products",productService.getAllProducts());
        return modelAndView;
    }

    @PostMapping("/addToCart")
    public ModelAndView addToCart(@ModelAttribute ShopDTO shopDTO) throws EmptyObject, CartFull {

        shopDTO = shopViewService.addToCart(shopDTO);


        ModelAndView modelAndView = new ModelAndView("continueShopping");
        setModelAndView(shopDTO, modelAndView);
        return modelAndView;
    }



    @PostMapping("/removeFromCart")
    public ModelAndView removeFromCart(@ModelAttribute ShopDTO shopDTO) throws EmptyObject, CartEmpty {

        shopDTO = shopViewService.removeFromCart(shopDTO);

        ModelAndView modelAndView = new ModelAndView("continueShopping");
        setModelAndView(shopDTO, modelAndView);
        return modelAndView;
    }


    @PostMapping("/addDiscount")
    public ModelAndView addDiscount(@ModelAttribute ShopDTO shopDTO) throws EmptyObject, InvalidDiscount {

        shopDTO = shopViewService.addDiscount(shopDTO);

        ModelAndView modelAndView = new ModelAndView("continueShopping");
        setModelAndView(shopDTO, modelAndView);
        return modelAndView;
    }



    @PostMapping("/placeOrder")
    public ModelAndView placeOrder(@ModelAttribute ShopDTO shopDTO) throws CartEmpty, EmptyObject {

        OrderDTO orderDTO = shopViewService.placeOrder(shopDTO);

        ModelAndView modelAndView = new ModelAndView("checkOut");
        modelAndView.addObject("shopOrder",orderDTO);
        return modelAndView;
    }

    @PostMapping("/orderSummary")
    public ModelAndView orderSummary(@ModelAttribute OrderDTO orderDTO){

        ModelAndView modelAndView = new ModelAndView("orderSummary");
        modelAndView.addObject("order",orderDTO);

        return modelAndView;
    }

    private void setModelAndView(ShopDTO shopDTO, ModelAndView modelAndView) throws EmptyObject {
        modelAndView.addObject("shopOrder", shopDTO);
        modelAndView.addObject("products",productService.getAllProducts());
        modelAndView.addObject("cart",shopViewService.cartList(shopDTO));
    }
}
