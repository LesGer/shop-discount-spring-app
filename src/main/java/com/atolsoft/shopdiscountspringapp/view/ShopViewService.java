package com.atolsoft.shopdiscountspringapp.view;

import com.atolsoft.shopdiscountspringapp.controller.BaseController;
import com.atolsoft.shopdiscountspringapp.controller.dto.CartDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.OrderDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.ShopDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.CartFull;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.exeptions.InvalidDiscount;
import com.atolsoft.shopdiscountspringapp.service.OrderService;
import com.atolsoft.shopdiscountspringapp.service.ProductService;
import com.atolsoft.shopdiscountspringapp.service.discount.DiscountType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShopViewService extends BaseController{

    @Autowired
    OrderService orderService;
    @Autowired
    ProductService productService;

    public ShopDTO addToCart(ShopDTO dto) throws CartFull, EmptyObject {

        if(dto.getOrderDTO() == null){
            dto.setOrderDTO(emptyOrderDTO());
        }

        if(dto.getVar() == 0){
            return dto;
        }

        dto.setOrderDTO(orderService.addToCart(dto.getOrderDTO(),((int) dto.getVar())));

        return setDTOValues(dto);
    }

    public ShopDTO removeFromCart(ShopDTO dto) throws CartEmpty, EmptyObject {

        if(dto.getVar() == 0){
            return dto;
        }

        dto.setOrderDTO(orderService.removeFromCart(dto.getOrderDTO(),((int) dto.getVar())-1));

        return setDTOValues(dto);
    }


    public List<ProductDTO> cartList(ShopDTO dto) throws EmptyObject {

        List<ProductDTO> cart = new ArrayList<>();
        List<ProductDTO> productDTOS = dto.getOrderDTO().getCart().getProductList();

        for(ProductDTO product : productDTOS) {
            cart.add(productService.findByProductId(productDTOS.get(productDTOS.indexOf(product)).getProductId()));
        }

        return cart;
    }

    public ShopDTO addDiscount(ShopDTO dto) throws EmptyObject, InvalidDiscount {

        dto.setOrderDTO(orderService.addDiscount(dto.getOrderDTO(),dto.getVar()));

        return dto;
    }

    public OrderDTO placeOrder(ShopDTO dto) throws CartEmpty, EmptyObject {

        dto.getOrderDTO().setDiscountType(DiscountType.PROPORTIONAL.getName());
        dto.getOrderDTO().getCart().setProductList(cartList(dto));

        return orderService.placeOrder(dto.getOrderDTO());
    }



    private OrderDTO emptyOrderDTO(){
        return OrderDTO.builder()
                .orderValue(0)
                .discount(0)
                .cart(
                        CartDTO.builder()
                                .productList(new ArrayList<>())
                                .cartValue(0)
                                .build()
                )
                .build();
    }

    private ShopDTO setDTOValues(ShopDTO shopDTO) {
        shopDTO.setVar(0);

        return shopDTO;
    }

}
