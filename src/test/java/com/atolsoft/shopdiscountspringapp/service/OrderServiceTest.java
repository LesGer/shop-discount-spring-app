package com.atolsoft.shopdiscountspringapp.service;


import com.atolsoft.shopdiscountspringapp.controller.dto.CartDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.OrderDTO;
import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.CartEmpty;
import com.atolsoft.shopdiscountspringapp.exeptions.CartFull;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.exeptions.InvalidDiscount;
import com.atolsoft.shopdiscountspringapp.model.Cart;
import com.atolsoft.shopdiscountspringapp.model.Order;
import com.atolsoft.shopdiscountspringapp.model.products.Lamp;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.repository.OrderRepository;
import com.atolsoft.shopdiscountspringapp.repository.ProductRepository;
import com.atolsoft.shopdiscountspringapp.service.factory.DiscountFactory;
import com.atolsoft.shopdiscountspringapp.service.discount.DiscountType;
import com.atolsoft.shopdiscountspringapp.service.factory.ProductFactory;
import com.atolsoft.shopdiscountspringapp.service.mapper.CartMapper;
import com.atolsoft.shopdiscountspringapp.service.mapper.OrderMapper;
import com.atolsoft.shopdiscountspringapp.service.mapper.ProductMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {


	@Spy
	DiscountFactory discount = new DiscountFactory();
	@Spy
	ProductFactory prodyctFactory = new ProductFactory();

	@Spy
	@InjectMocks
	OrderMapper orderMapper = new OrderMapper();
	@Spy
	@InjectMocks
	CartMapper cartMapper = new CartMapper();
	@Spy
	@InjectMocks
	ProductMapper productMapper = new ProductMapper();

	@Mock
	OrderRepository orderRepository;
	@Mock
	ProductRepository productRepository;
	@Mock
	ProductService productService;

	@InjectMocks
	OrderService orderService;

	@Test
	public void getAllOrders_empty(){

		when(orderRepository.findAll()).thenReturn(new ArrayList<>());

		List<OrderDTO> orderDTOList = orderService.getAllOrders();

		verify(orderRepository,times(1)).findAll();
		assertEquals(0,orderDTOList.size());

	}

	@Test
	public void getAllOrders_4objects(){

		List<Order> orders = new ArrayList<>();
		for(int i=0; i<4; i++) {
			orders.add(testOrder());
		}

		when(orderRepository.findAll()).thenReturn(orders);

		List<OrderDTO> orderDTOList = orderService.getAllOrders();

		verify(orderRepository,times(1)).findAll();
		assertEquals(4,orderDTOList.size());

	}


	@Test
	public void addToCart_1Product() throws CartFull, EmptyObject{

		List<ProductDTO> products = new ArrayList<>();



		OrderDTO order = testOrderDTO(products);

		order = orderService.addToCart(order, lampDTO(200.00));
		
		assertEquals(1, products.size());
		assertEquals(200.00, order.getCart().getCartValue(),0.001);
		
	}
	
	@Test
	public void addToCart_3Product() throws CartFull, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);
		
		order = orderService.addToCart(order, lampDTO(200.00));
		order = orderService.addToCart(order, vegetableDTO(15.00));
		order = orderService.addToCart(order, lampDTO(120.00));
		
		assertEquals(3, products.size());
		assertEquals(335.00, order.getCart().getCartValue(),0.001);
		
	}
	
	@Test
	public void addToCart_5Product() throws CartFull, EmptyObject{

		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);
		
		order = orderService.addToCart(order, lampDTO(200.00));
		order = orderService.addToCart(order, vegetableDTO(15.00));
		order = orderService.addToCart(order, lampDTO(120.00));
		order = orderService.addToCart(order, vegetableDTO(5.99));
		order = orderService.addToCart(order, lampDTO(0.62));
		
		assertEquals(5, products.size());
		assertEquals(341.61, order.getCart().getCartValue(),0.001);
		
	}
	
	@Test(expected=CartFull.class)
	public void addToCart_6Product_throwCartFull() throws CartFull, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);
		
		order = orderService.addToCart(order, lampDTO(200.00));
		order = orderService.addToCart(order, vegetableDTO(15.00));
		order = orderService.addToCart(order, lampDTO(120.00));
		order = orderService.addToCart(order, vegetableDTO(5.99));
		order = orderService.addToCart(order, lampDTO(0.62));
		order = orderService.addToCart(order, lampDTO(0.62));

		
	}
	
	@Test(expected=EmptyObject.class)
	public void addToCart_nullOrder() throws CartFull, EmptyObject{
			
		OrderDTO order = null;

		order = orderService.addToCart(order, lampDTO(200));
		
	}
	
	@Test(expected=EmptyObject.class)
	public void addToCart_nullProduct() throws CartFull, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		order = orderService.addToCart(order, null);
		
	}

	@Test(expected = CartFull.class)
	public void addToCart_6Product_id() throws CartFull, EmptyObject{

		int id = 1;

		when(productService.findByProductId(id)).thenReturn(lampDTO(200));

		List<ProductDTO> products = new ArrayList<>();
		for(int i=0 ; i<6; i++) {
			products.add(vegetableDTO(200));
		}

		OrderDTO order = testOrderDTO(products);

		orderService.addToCart(order, id);
	}

	@Test(expected = EmptyObject.class)
	public void addToCart_1Product_id_NullOrder() throws CartFull, EmptyObject{

		int id = 1;

		when(productService.findByProductId(id)).thenReturn(lampDTO(200));

		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		orderService.addToCart(null, id);
	}

	@Test(expected = EmptyObject.class)
	public void addToCart_1Product_id_0index() throws CartFull, EmptyObject{

		int id = 0;

		when(productService.findByProductId(id)).thenReturn(lampDTO(200));

		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		orderService.addToCart(order, id);
	}

	@Test
	public void addToCart_1Product_id() throws CartFull, EmptyObject{

		int id = 1;

		when(productService.findByProductId(id)).thenReturn(lampDTO(200));

		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		order = orderService.addToCart(order, id);

		assertEquals(products.size(),1);
		assertEquals(order.getCart().getCartValue(),200d,0.001);

	}

	@Test
	public void removeFromCart_3Products() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));
		products.add(lampDTO(150.00));
		products.add(lampDTO(100.00));

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, lampDTO(150));
		
		assertEquals(2,products.size());
		assertEquals(300,order.getCart().getCartValue(),0.001);
		
		
	}
	
	@Test
	public void removeFromCart_5Products() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));
		products.add(lampDTO(150.00));
		products.add(lampDTO(100.00));
		products.add(lampDTO(354.00));
		products.add(lampDTO(126.00));

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, lampDTO(150));
		
		assertEquals(4,products.size());
		assertEquals(780,order.getCart().getCartValue(),0.001);
		
		
	}
	
	@Test
	public void removeFromCart_1Product() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));


		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, lampDTO(200));
		
		assertEquals(0,products.size());
		assertEquals(0,order.getOrderValue(),0.001);
		
		
	}
	
	@Test(expected=CartEmpty.class)
	public void removeFromCart_0Products() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, lampDTO(200));

	}
	
	@Test(expected=EmptyObject.class)
	public void removeFromCart_1Products_nullOrder() throws CartEmpty, EmptyObject{
		
		OrderDTO order = null;

		order = orderService.removeFromCart(order, lampDTO(200));

	}
	
	@Test(expected=EmptyObject.class)
	public void removeFromCart_1Products_nullProduct() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();	
		products.add(lampDTO(200.00));

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, null);
		
	}
	
	@Test
	public void removeFromCart_1Product_0position() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));


		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, 0);
		
		assertEquals(0,products.size());
		assertEquals(0,order.getCart().getCartValue(),0.001);
		
	}
	
	@Test
	public void removeFromCart_3Product_1position() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));
		products.add(lampDTO(200.00));
		products.add(lampDTO(150.00));


		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, 1);
		
		assertEquals(2,products.size());
		assertEquals(350,order.getCart().getCartValue(),0.001);
		
	}
	
	@Test(expected=EmptyObject.class)
	public void removeFromCart_3Product_5position() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));
		products.add(lampDTO(200.00));
		products.add(lampDTO(150.00));

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, 5);

	}
	
	@Test(expected=EmptyObject.class)
	public void removeFromCart_nullOrder_2position() throws CartEmpty, EmptyObject{
		
		OrderDTO order = null;

		order = orderService.removeFromCart(order, 2);
		
	}
	
	@Test(expected=CartEmpty.class)
	public void removeFromCart_EmptyList_2position() throws CartEmpty, EmptyObject{
		
		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeFromCart(order, 2);

	}
	
	@Test
	public void removeAllFromCart_5Products() throws EmptyObject, CartEmpty{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));
		products.add(lampDTO(150.00));
		products.add(lampDTO(100.00));
		products.add(lampDTO(354.00));
		products.add(lampDTO(126.00));

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeAllFromCart(order);
		
		assertEquals(0,products.size());
		assertEquals(0,order.getOrderValue(),0.001);
		
	}
	
	@Test
	public void removeAllFromCart_3Products() throws EmptyObject, CartEmpty{
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00));
		products.add(lampDTO(150.00));

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeAllFromCart(order);
		
		assertEquals(0,products.size());
		assertEquals(0,order.getOrderValue(),0.001);
	}
	
	@Test(expected=CartEmpty.class)
	public void removeAllFromCart_EmptyCart() throws EmptyObject, CartEmpty{
		
		List<ProductDTO> products = new ArrayList<>();

		OrderDTO order = testOrderDTO(products);

		order = orderService.removeAllFromCart(order);

	}
	
	@Test(expected=EmptyObject.class)
	public void removeAllFromCart_nullOrder() throws EmptyObject, CartEmpty{
		
		OrderDTO order = null;

		order = orderService.removeAllFromCart(order);
		
	}
	
	@Test
	public void addDiscount_100() throws EmptyObject, InvalidDiscount{
		
		double discount = 100.00d;
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00d));
		products.add(lampDTO(150.00d));

		OrderDTO order = testOrderDTO(products);

		order = orderService.addDiscount(order, discount);
		
		assertEquals(100,order.getDiscount(),0.001);
	}
	
	@Test(expected=InvalidDiscount.class)
	public void addDiscount_0() throws EmptyObject, InvalidDiscount{
		
		double discount = 0;
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00d));
		products.add(lampDTO(150.00d));

		OrderDTO order = testOrderDTO(products);

		order = orderService.addDiscount(order, discount);

	}
	
	@Test(expected=InvalidDiscount.class)
	public void addDiscount_minus1() throws EmptyObject, InvalidDiscount{
		
		double discount = -1;
		
		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00d));
		products.add(lampDTO(150.00d));

		OrderDTO order = testOrderDTO(products);

		order = orderService.addDiscount(order, discount);
		
	}
	
	@Test(expected=EmptyObject.class)
	public void addDiscount_nullOrder() throws EmptyObject, InvalidDiscount{
		
		double discount = -1;
		
		OrderDTO order = null;

		order = orderService.addDiscount(order, discount);
		
	}

	@Test
	public void countDiscount_properDiscount() throws CartEmpty, EmptyObject {

		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00d));
		products.add(lampDTO(150.00d));

		OrderDTO orderDTO = testOrderDTO(products);
		orderDTO.setDiscount(100.00d);
		orderDTO.getCart().setCartValue(350.00d);

		OrderDTO result = orderService.placeOrder(orderDTO);

		assertEquals(result.getCart().getCartValue(), orderDTO.getCart().getCartValue(), 0.001);
		assertEquals(250.00d,result.getOrderValue(),0.001);

	}

	@Test
	public void countDiscount_invalidDiscount() throws CartEmpty, EmptyObject {

		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00d));
		products.add(lampDTO(150.00d));

		OrderDTO orderDTO = testOrderDTO(products);
		orderDTO.setDiscount(0);
		orderDTO.getCart().setCartValue(350.00d);

		OrderDTO result = orderService.placeOrder(orderDTO);

		assertEquals(result.getCart().getCartValue(), orderDTO.getCart().getCartValue(), 0.001);
		assertEquals(350.00d,result.getOrderValue(),0.001);

	}

	@Test
	public void countDiscount_DiscountBiggerThanCartValue() throws CartEmpty, EmptyObject {

		List<ProductDTO> products = new ArrayList<>();
		products.add(lampDTO(200.00d));
		products.add(lampDTO(150.00d));

		OrderDTO orderDTO = testOrderDTO(products);
		orderDTO.setDiscount(400d);
		orderDTO.getCart().setCartValue(350.00d);

		OrderDTO result = orderService.placeOrder(orderDTO);

		assertEquals(result.getCart().getCartValue(), orderDTO.getCart().getCartValue(), 0.001);
		assertEquals(0.00d,result.getOrderValue(),0.001);

	}




	private OrderDTO testOrderDTO( List<ProductDTO> products){
		return OrderDTO.builder()
				.cart(CartDTO.builder().cartValue(0).productList(products).build())
				.orderValue(0)
				.discount(0)
				.discountType(DiscountType.PROPORTIONAL.getName())
				.build();
	}

	private ProductDTO lampDTO(double value){
		return ProductDTO.builder().name("lampa")
				.description("Taka lampa")
				.price(value)
				.type("Lamp")
				.build();
	}

	private Product testLamp(double value){
		return new Lamp(0,"lampa","testowa lampa",value);
	}
	
	private ProductDTO vegetableDTO(double value){
		return ProductDTO.builder().name("warzywo")
		.description("Taki por")
		.price(value)
		.type("Vegetable")
		.build();
	}

	private Order testOrder(){
		return Order.builder()
				.cart(
						Cart.builder()
						.productList(
								new ArrayList<>()
						)
						.build()
				)
				.build();
	}
}
