package com.atolsoft.shopdiscountspringapp.service;

import com.atolsoft.shopdiscountspringapp.controller.dto.ProductDTO;
import com.atolsoft.shopdiscountspringapp.exeptions.EmptyObject;
import com.atolsoft.shopdiscountspringapp.model.products.Lamp;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.model.products.ProductType;
import com.atolsoft.shopdiscountspringapp.model.products.Vegetable;
import com.atolsoft.shopdiscountspringapp.repository.ProductRepository;
import com.atolsoft.shopdiscountspringapp.service.factory.ProductFactory;
import com.atolsoft.shopdiscountspringapp.service.mapper.ProductMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Spy
    ProductFactory productFactory = new ProductFactory();
    @Spy
    @InjectMocks
    ProductMapper productMapper = new ProductMapper();
    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductService productService;

    @Captor
    private ArgumentCaptor<Product> productCaptor;


    @Test
    public void getAllProducts_goodList() {

        when(productRepository.findAll()).thenReturn(productList());

        List<ProductDTO> result = productService.getAllProducts();

        verify(productRepository, times(1)).findAll();
        assertEquals(2,result.size());

    }

    @Test
    public void getAllProducts_emptyList() {

        when(productRepository.findAll()).thenReturn(new ArrayList<>());

        List<ProductDTO> result = productService.getAllProducts();

        verify(productRepository, times(1)).findAll();
        assertEquals(0,result.size());

    }

    @Test
    public void getAllProductsByType_emptyList() {

        String type = "Lamp";

        when(productRepository.findAllByType(type)).thenReturn(new ArrayList<>());

        List<ProductDTO> result = productService.getAllProductsByType(type);

        verify(productRepository, times(1)).findAllByType(type);
        assertEquals(0,result.size());

    }

    @Test
    public void getAllProductsByType_goodList() {

        String type = "Lamp";

        when(productRepository.findAllByType(type)).thenReturn(sameProductList());

        List<ProductDTO> result = productService.getAllProductsByType(type);

        verify(productRepository, times(1)).findAllByType(type);
        assertEquals(2,result.size());

    }

    @Test
    public void findByProductId() throws EmptyObject {

        int id = 5;

        when(productRepository.findOne(id)).thenReturn(lamp());

        ProductDTO result = productService.findByProductId(id);

        verify(productRepository,times(1)).findOne(id);

    }

    @Test(expected = EmptyObject.class)
    public void findByProductId_EmptyObject() throws EmptyObject {

        int id = 5;

        when(productRepository.findOne(id)).thenReturn(null);

        ProductDTO result = productService.findByProductId(id);

    }

    @Test(expected = EmptyObject.class)
    public void findByProductId_Id0() throws EmptyObject {

        int id = 0;

        when(productRepository.findOne(id)).thenReturn(lamp());

        ProductDTO result = productService.findByProductId(id);

    }

    @Test(expected = EmptyObject.class)
    public void findByProductId_IdMinus() throws EmptyObject {

        int id = -7;

        when(productRepository.findOne(id)).thenReturn(lamp());

        ProductDTO result = productService.findByProductId(id);

    }

    @Test
    public void saveProduct_lamp() throws EmptyObject {

        productService.saveProduct(lampDTO());

        verify(productRepository,times(1)).save(productCaptor.capture());
        assertEquals("lampa",productCaptor.getValue().getName());
        assertThat(productCaptor.getValue()).isInstanceOf(Lamp.class);

    }
    @Test
    public void saveProduct_vege() throws EmptyObject {

        productService.saveProduct(vegeDTO());

        verify(productRepository,times(1)).save(productCaptor.capture());
        assertEquals("por",productCaptor.getValue().getName());
        assertThat(productCaptor.getValue()).isInstanceOf(Vegetable.class);

    }

    @Test(expected = EmptyObject.class)
    public void saveProduct_nullObject() throws EmptyObject {

        productService.saveProduct(null);

    }

    @Test
    public void updateProduct() throws EmptyObject {

        int id = 1;
        when(productRepository.findOne(id)).thenReturn(lamp());

        productService.updateProduct(lampDTO(),id);

        verify(productRepository,times(1)).findOne(id);
        verify(productRepository,times(1)).save(productCaptor.capture());

         assertThat(productCaptor.getValue()).isInstanceOf(Lamp.class);

    }

    @Test(expected = EmptyObject.class)
    public void updateProduct_nullFind() throws EmptyObject {

        int id = 1;

        when(productRepository.findOne(id)).thenReturn(null);

        productService.updateProduct(vegeDTO(),id);


    }

    @Test(expected = EmptyObject.class)
    public void updateProduct_nullFind_badIndex() throws EmptyObject {

        int id = -6;

        when(productRepository.findOne(id)).thenReturn(null);

        productService.updateProduct(vegeDTO(),id);


    }

    @Test(expected = EmptyObject.class)
    public void updateProduct_nullDTO() throws EmptyObject {

        int id = 1;

        when(productRepository.findOne(id)).thenReturn(lamp());

        productService.updateProduct(null,id);
    }

    @Test
    public void deleteProduct() throws EmptyObject {

        ProductDTO dto = lampDTO();

        when(productRepository.findOne(dto.getProductId())).thenReturn(lamp());

        productService.deleteProduct(dto);

        verify(productRepository,times(1)).delete(productCaptor.capture());
        assertThat(productCaptor.getValue()).isInstanceOf(Lamp.class);
        assertEquals(1,productCaptor.getValue().getProductId());

    }

    @Test(expected = EmptyObject.class)
    public void deleteProduct_throwEmptyObject_nullInput() throws EmptyObject {

        ProductDTO dto = null;

        when(productRepository.findOne(1)).thenReturn(lamp());

        productService.deleteProduct(dto);


    }

    @Test(expected = EmptyObject.class)
    public void deleteProduct_throwEmptyObject_nullFind() throws EmptyObject {

        ProductDTO dto = lampDTO();

        when(productRepository.findOne(dto.getProductId())).thenReturn(null);

        productService.deleteProduct(dto);


    }

    @Test
    public void deleteProduct_index_properId() throws EmptyObject {

        int id = 5;

        when(productRepository.findOne(id)).thenReturn(lamp());

        productService.deleteProduct(id);

        verify(productRepository,times(1)).delete(productCaptor.capture());
        assertThat(productCaptor.getValue()).isInstanceOf(Lamp.class);
        assertEquals(1,productCaptor.getValue().getProductId());

    }

    @Test(expected = EmptyObject.class)
    public void deleteProduct_index_throwEmptyObject_badIndex() throws EmptyObject {

        int id = 0;

        when(productRepository.findOne(id)).thenReturn(lamp());

        productService.deleteProduct(id);


    }

    @Test(expected = EmptyObject.class)
    public void deleteProduct_index_throwEmptyObject_badIndex2() throws EmptyObject {

        int id = -5;

        when(productRepository.findOne(id)).thenReturn(lamp());

        productService.deleteProduct(id);


    }

    @Test(expected = EmptyObject.class)
    public void deleteProduct_index_throwEmptyObject_nullFind() throws EmptyObject {

        int id = 5;

        when(productRepository.findOne(id)).thenReturn(null);

        productService.deleteProduct(id);


    }



    private List<Product> productList() {
        List<Product> products = new ArrayList<>();

        products.add(new Lamp(1,"lampa","opis",200));
        products.add(new Vegetable(2,"por","opis",15.5));

        return products;
    }

    private List<Product> sameProductList() {
        List<Product> products = new ArrayList<>();

        products.add(new Lamp(1,"lampa","opis",200));
        products.add(new Lamp(2,"lampa","opis",153));

        return products;
    }

    private Product lamp(){
        return new Lamp(1,"lampa","opis",200);
    }

    private ProductDTO lampDTO(){
        return  ProductDTO.builder()
                .productId(1)
                .name("lampa")
                .description("opis")
                .type(ProductType.LAMP.getType())
                .price(200d).build();
    }

    private ProductDTO vegeDTO(){
        return  ProductDTO.builder()
                .productId(1)
                .name("por")
                .description("opis")
                .type(ProductType.VEGETABLE.getType())
                .price(200d).build();
    }
}