package com.atolsoft.shopdiscountspringapp.service.discount;

import com.atolsoft.shopdiscountspringapp.model.Cart;
import com.atolsoft.shopdiscountspringapp.model.Order;
import com.atolsoft.shopdiscountspringapp.model.products.Lamp;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.model.products.Vegetable;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProportionalTest {


    Proportional proportional = new Proportional();

    @Test
    public void countDiscount_5Products_100Discount() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(200.00));
        products.add(lamp(100.00));
        products.add(lamp(50.00));
        products.add(lamp(50.00));
        products.add(lamp(100.00));


        Order order = Order.builder()
                .cart(Cart.builder().cartValue(500).productList(products).build())
                .orderValue(500)
                .discount(100)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(40, products.get(0).getDiscountValue(), 0.001);
        assertEquals(20, products.get(1).getDiscountValue(), 0.001);
        assertEquals(10, products.get(2).getDiscountValue(), 0.001);
        assertEquals(10, products.get(3).getDiscountValue(), 0.001);
        assertEquals(20, products.get(4).getDiscountValue(), 0.001);
        assertEquals(400, order.getOrderValue(), 0.001);

    }

    @Test
    public void countDiscount_4Products_100Discount() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(200.00));
        products.add(lamp(100.00));
        products.add(lamp(50.00));
        products.add(lamp(50.00));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(400).productList(products).build())
                .orderValue(400)
                .discount(100)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(50, products.get(0).getDiscountValue(), 0.001);
        assertEquals(25, products.get(1).getDiscountValue(), 0.001);
        assertEquals(12.5, products.get(2).getDiscountValue(), 0.001);
        assertEquals(12.5, products.get(3).getDiscountValue(), 0.001);
        assertEquals(300, order.getOrderValue(), 0.001);

    }

    @Test
    public void countDiscount_3Products_01Discount() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(0.20));
        products.add(lamp(0.10));
        products.add(lamp(0.05));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(0.35).productList(products).build())
                .orderValue(0.35)
                .discount(0.1)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(0.06, products.get(0).getDiscountValue(), 0.001);
        assertEquals(0.03, products.get(1).getDiscountValue(), 0.001);
        assertEquals(0.01, products.get(2).getDiscountValue(), 0.001);
        assertEquals(0.25, order.getOrderValue(), 0.001);
    }

    @Test
    public void countDiscount_3Products_equalPrice_10Discount() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(20.00));
        products.add(lamp(20.00));
        products.add(lamp(20.00));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(60).productList(products).build())
                .orderValue(60)
                .discount(10)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(3.33, products.get(0).getDiscountValue(), 0.001);
        assertEquals(3.33, products.get(1).getDiscountValue(), 0.001);
        assertEquals(3.34, products.get(2).getDiscountValue(), 0.001);
        assertEquals(50, order.getOrderValue(), 0.001);

    }

    @Test
    public void countDiscount_3Products_equalPrice_DiscountPrimeNumber() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(7.00));
        products.add(lamp(7.00));
        products.add(lamp(7.00));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(21).productList(products).build())
                .orderValue(21)
                .discount(4)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(1.33, products.get(0).getDiscountValue(), 0.001);
        assertEquals(1.33, products.get(1).getDiscountValue(), 0.001);
        assertEquals(1.34, products.get(2).getDiscountValue(), 0.001);
        assertEquals(17, order.getOrderValue(), 0.001);

    }

    @Test
    public void countDiscount_2Products_10Discount() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(78.99));
        products.add(lamp(42.99));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(121.98).productList(products).build())
                .orderValue(121.98)
                .discount(10)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(6.48, products.get(0).getDiscountValue(), 0.001);
        assertEquals(3.52, products.get(1).getDiscountValue(), 0.001);
        assertEquals(111.98, order.getOrderValue(), 0.001);

    }

    @Test
    public void countDiscount_1Products_10Discount() {

        List<Product> products = new ArrayList<>();
        products.add(lamp(78.99));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(78.99).productList(products).build())
                .orderValue(78.99)
                .discount(10)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(10, products.get(0).getDiscountValue(), 0.001);
        assertEquals(68.99, order.getOrderValue(), 0.001);


    }

    @Test
    public void countDiscount_2Products_10Discount_diferentProducts() {

        List<Product> products = new ArrayList<>();
        products.add(vegetable(1.24));
        products.add(lamp(42.99));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(44.23).productList(products).build())
                .orderValue(44.23)
                .discount(10)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(0.28, products.get(0).getDiscountValue(), 0.001);
        assertEquals(9.72, products.get(1).getDiscountValue(), 0.001);
        assertEquals(34.23, order.getOrderValue(), 0.001);

    }


    @Test
    public void countDiscount_DiscountLowerThanCartValue_checkCartValue_normalValues() {
        List<Product> products = new ArrayList<>();
        products.add(vegetable(1.24));
        products.add(lamp(42.99));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(44.23).productList(products).build())
                .orderValue(44.23)
                .discount(40)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(4.23, order.getOrderValue(), 0.001);
    }

    @Test
    public void countDiscount_DiscountLowerThanCartValue_checkCartValue_normalValues2() {
        List<Product> products = new ArrayList<>();
        products.add(vegetable(100.00));
        products.add(lamp(50.00));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(150.00).productList(products).build())
                .orderValue(150.00)
                .discount(123.52)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(26.48, order.getOrderValue(), 0.001);
        assertEquals(150.00, order.getCart().getCartValue(), 0.001);
    }

    @Test
    public void countDiscount_DiscountLowerThanCartValue_checkCartValue_lowValues() {
        List<Product> products = new ArrayList<>();
        products.add(vegetable(0.22));
        products.add(lamp(0.15));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(0.37).productList(products).build())
                .orderValue(0.37)
                .discount(0.30)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(0.07, order.getOrderValue(), 0.001);
    }

    @Test
    public void countDiscount_DiscountLowerThanCartValue_checkCartValue_lowValues2() {
        List<Product> products = new ArrayList<>();
        products.add(vegetable(0.03));
        products.add(lamp(0.01));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(0.04).productList(products).build())
                .orderValue(0.04)
                .discount(0.03)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(0.01, order.getOrderValue(), 0.001);
        assertEquals(0.04, order.getCart().getCartValue(), 0.001);
    }

    @Test
    public void countDiscount_DiscountLowerThanCartValue_checkCartValue_highValues() {
        List<Product> products = new ArrayList<>();
        products.add(vegetable(100000));
        products.add(lamp(500000));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(600000).productList(products).build())
                .orderValue(600000)
                .discount(100)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(599900, order.getOrderValue(), 0.001);
        assertEquals(600000, order.getCart().getCartValue(), 0.001);
    }

    @Test
    public void countDiscount_DiscountLowerThanCartValue_checkCartValue_highValues2() {
        List<Product> products = new ArrayList<>();
        products.add(vegetable(1000000));
        products.add(lamp(50000000));

        Order order = Order.builder()
                .cart(Cart.builder().cartValue(5100000).productList(products).build())
                .orderValue(5100000)
                .discount(0.01)
                .build();

        order = proportional.countDiscount(order);

        assertEquals(5099999.99, order.getOrderValue(), 0.001);
        assertEquals(5100000, order.getCart().getCartValue(), 0.001);
    }

    private Product lamp(double value) {
        return new Lamp(1, "lampa", "Taka lampa", value);

    }

    private Product vegetable(double value) {
        return new Vegetable(1, "warzywo", "Taki por", value);
    }


}
