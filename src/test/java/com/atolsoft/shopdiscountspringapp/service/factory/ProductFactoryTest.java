package com.atolsoft.shopdiscountspringapp.service.factory;


import com.atolsoft.shopdiscountspringapp.model.products.Lamp;
import com.atolsoft.shopdiscountspringapp.model.products.Product;
import com.atolsoft.shopdiscountspringapp.model.products.ProductType;
import com.atolsoft.shopdiscountspringapp.model.products.Vegetable;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


public class ProductFactoryTest {

    @Test
    public void createProductFromType_Lamp() {

        String type = ProductType.LAMP.getType();

        ProductFactory productFactory = new ProductFactory();

        Product testProduct = productFactory.createProductFromType(type);

        assertThat(testProduct).isInstanceOf(Lamp.class);
        assertEquals(type,testProduct.getType());

    }

    @Test
    public void createProductFromType_vege() {

        String type = ProductType.VEGETABLE.getType();

        ProductFactory productFactory = new ProductFactory();

        Product testProduct = productFactory.createProductFromType(type);

        assertThat(testProduct).isInstanceOf(Vegetable.class);

    }

    @Test
    public void createProductFromType_dud() {

        String type = "something";

        ProductFactory productFactory = new ProductFactory();

        Product testProduct = productFactory.createProductFromType(type);

        assertThat(testProduct).isNull();

    }
}